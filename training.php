<?php

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     testing
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 *
 */
	define('ENVIRONMENT', 'development');

?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Programs</title>
<link href="layout.css" rel="stylesheet" type="text/css">
</head>

<body class="body">

    <?php 
        $title="Training";
        require_once('header.php');
    ?>

    <div style="width:100%; height:60%;">
        <table align="center" cellspacing="3" cellpadding="3" style="table-layout:fixed; width: 100%;">
            <tr style="height: 100px;"></tr>
            <tr>
                <td></td>
                <td></td>
                <td align="center">
                    <img src="training.png" alt="" id="mainbutton"/>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td align="center" style="margin: 10px;"><strong>Training</strong><br/>Training & Development</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
	</div>
	
    <?php 
        require_once('footer.php');
    ?>
</body>
</html>

<php?
?>

