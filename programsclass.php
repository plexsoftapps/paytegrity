<?php 
require_once('dbutils.php');

class Programs {  
     
    private $programs = array();

    /**
     * Constructor
     */
    public function __construct(){     
        // load the programs
        $numprograms = $this->Load();
        error_log("Programs loaded = " . $numprograms);
    }
     
    public function Load()
    {
        $programs = array();

        // get the records from the db
        // error_log("Start of getting programs");

        // open the db
        $dbutils = new dbUtils();

        // error_log("Opening database");

        $dbutils->openDB("","","","");

        // error_log("Selecting records");

        $rs = $dbutils->select("Select * From Folders");

        // error_log("Checking recordset");

		while ($row = mysqli_fetch_assoc($rs))
		{
            // error_log("Walking row");
            $program = $row["id"] . "~" . $row["folder"] . "~" . $row["link_page"] . "~";

            // error_log("Program = " . $program);

            $programs[] = $program;
		}

        // // close the db
        // error_log("Closeing database");

        $dbutils->closeDB();

        $this->programs = $programs;

        return count($programs);
    }

    public function GetPrograms()
    {
        return $this->programs;
    }
}
?>
