<?php

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     testing
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 *
 */
	define('ENVIRONMENT', 'development');

?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Programs</title>
<link href="layout.css" rel="stylesheet" type="text/css">
<link href="styles.css" rel="stylesheet" type="text/css">
</head>

<body style="background-color:aliceblue">

    <?php 
        $title="Behaviorial Health Report";
        $showbuttons="YES";
        require_once('header.php');
    ?>

    <?php
        // get the queries
        require_once('programsclass.php');
        $programsobject = new Programs();
        $programs = $programsobject->GetPrograms();  //array("Test1", "Test2", "Test3");  //
        $numcols = 4;
        $numrows = count($programs) / $numcols;
        $num_programs = count($programs);

        $recs = BehavioralHealthRecords();
        $numrecs = count($recs);
        $header = $recs[0];
        $headers = explode("~",$header); 
?>

    <br/>

    <div style="width:100%; height:80%;">
        <table border="1" style="width:95%; height:20px;" cellpadding="10,10,10,10">

            <tr style="height: 100%; color:blue; background-color:lightblue;">
                <td style="width:30%; font-size:26px; text-align:left;">
                Folders
                </td>
                <td style="width:70%; font-size:26px; text-align:left;">
                <?php
                    // the first row of the recs array has the columns
                    // create the table
                    echo('<table border="0">');
                    echo('  <colgroup>');
                    echo('    <col style="width:30px;">');
                    for ($i=0;$i<count($headers);$i++)
                    {
                        $i = $i + 1;
                        $colwidth = $headers[$i];
                        echo('    <col style="width:' . $colwidth . ';">');
                    }
        
                    echo('  </colgroup>');
                    echo('  <tr>');
                    echo('    <td style="style="text-align:center; "><input type="checkbox" class="largerCheckbox" id="selected" name="selected" value=""></td>');
                    for ($i=0;$i<count($headers);$i++)
                    {
                        echo('<td>' . $headers[$i] . '</td>');
                        $i = $i + 1;
                    }
                    echo('  </tr>');
                    echo('</table>');
                ?>
                </td>
            </tr>

        </table>

		<table border="1" style="width:95%; height:80%;" cellspacing="5" cellpadding="5" class="fixed-table">
			<tr>
				<td style="width:30%">
                    <div style="overflow-y:scroll; overflow-x: hidden; height:100%">
                        <?php
                            // show all the folder items
                            for ($i = 0; $i < $num_programs; $i++) 
                            {
                                $program = $programs[$i];
                                $parts = explode("~", $program);
                                if (count($parts) > 2)
                                {
                                    $link = $parts[2];
                                }
                                else 
                                {
                                    $link = "queriesnotyet.php?q=" . $parts[1];
                                }

                                echo('<a href="' . $baseurl . '/' . $link . '">' .
                                            $parts[1] . ' Queries <br/><br/>
                                        </a>');
                                
                            }
                        ?>
                    </div>
				</td>

				<td style="width:70%">
                    <div style="overflow-y:scroll; overflow-x: hidden; height:100%">
                        <?php
                            echo('<table border="0">');
                            echo('  <colgroup>');
                            echo('    <col style="width:30px;">');
                            for ($i=0;$i<count($headers);$i++)
                            {
                                $i = $i + 1;
                                $colwidth = $headers[$i];
                                echo('    <col style="width:' . $colwidth . ';">');
                            }
                            echo('  </colgroup>');
                                        
                            // show the report for the folder item selected
                            for ($i=1;$i<count($recs);$i++)
                            {
                                echo('  <tr style="height:25px;">');
                                echo('    <td><input type="checkbox" class="largerCheckbox" id="selected" name="selected" value=""></td>');
                                echo('  <td>');
                                    echo($i+100);
                                echo('  </td>');
                                $detail = explode("~", $recs[$i]);
                                for ($j = 1;$j < count($detail); $j++)
                                {
                                    echo('  <td>');
                                    echo($detail[$j]);
                                    echo('  </td>');
                                }
                                echo('  </tr>');
                            }

                            echo('</table>');
                        ?>
                    </div>
				</td>
			</tr>
		</table>
	</div>
	
    <?php 
        require_once('footer.php');
    ?>
</body>
</html>

<?php

function BehavioralHealthRecords()
{
    $recs = array();

    $recs[] = "ID~40px~Title~600px~Created By~150px~Last Updated~150px";
    $recs[] = "100~MS-DRG Coding Validation HIV~Srinivas~7/5/2021";
    $recs[] = "101~MS-DRG Coding Validation Sepsis~Srinivas~7/5/2021";
    $recs[] = "102~MS-DRG Coding Validation MRSA~Srinivas~7/5/2021";
    $recs[] = "103~MS-DRG Coding Validation Coronavirus~Srinivas~7/5/2021";
    $recs[] = "104~MS-DRG Coding Validation Pneumonia~Srinivas~7/5/2021";
    $recs[] = "105~MS-DRG Coding Validation Acute Bronchitis~Srinivas~7/5/2021";
    $recs[] = "106~MS-DRG Coding Validation Lower Respiratory Infection~Srinivas~7/5/2021";
    $recs[] = "107~MS-DRG Coding Validation Acute Respiratory Distress Syn~Srinivas~7/5/2021";
    $recs[] = "108~MS-DRG Coding Validation Acute Respiratory Failure~Srinivas~7/5/2021";
    $recs[] = "109~MS-DRG Coding Validation HIV~Srinivas~7/5/2021";
    $recs[] = "100~MS-DRG Coding Validation HIV~Srinivas~7/5/2021";
    $recs[] = "101~MS-DRG Coding Validation Sepsis~Srinivas~7/5/2021";
    $recs[] = "102~MS-DRG Coding Validation MRSA~Srinivas~7/5/2021";
    $recs[] = "103~MS-DRG Coding Validation Coronavirus~Srinivas~7/5/2021";
    $recs[] = "104~MS-DRG Coding Validation Pneumonia~Srinivas~7/5/2021";
    $recs[] = "105~MS-DRG Coding Validation Acute Bronchitis~Srinivas~7/5/2021";
    $recs[] = "106~MS-DRG Coding Validation Lower Respiratory Infection~Srinivas~7/5/2021";
    $recs[] = "107~MS-DRG Coding Validation Acute Respiratory Distress Syn~Srinivas~7/5/2021";
    $recs[] = "108~MS-DRG Coding Validation Acute Respiratory Failure~Srinivas~7/5/2021";
    $recs[] = "109~MS-DRG Coding Validation HIV~Srinivas~7/5/2021";
    $recs[] = "100~MS-DRG Coding Validation HIV~Srinivas~7/5/2021";
    $recs[] = "101~MS-DRG Coding Validation Sepsis~Srinivas~7/5/2021";
    $recs[] = "102~MS-DRG Coding Validation MRSA~Srinivas~7/5/2021";
    $recs[] = "103~MS-DRG Coding Validation Coronavirus~Srinivas~7/5/2021";
    $recs[] = "104~MS-DRG Coding Validation Pneumonia~Srinivas~7/5/2021";
    $recs[] = "105~MS-DRG Coding Validation Acute Bronchitis~Srinivas~7/5/2021";
    $recs[] = "106~MS-DRG Coding Validation Lower Respiratory Infection~Srinivas~7/5/2021";
    $recs[] = "107~MS-DRG Coding Validation Acute Respiratory Distress Syn~Srinivas~7/5/2021";
    $recs[] = "108~MS-DRG Coding Validation Acute Respiratory Failure~Srinivas~7/5/2021";
    $recs[] = "109~MS-DRG Coding Validation HIV~Srinivas~7/5/2021";
    $recs[] = "100~MS-DRG Coding Validation HIV~Srinivas~7/5/2021";
    $recs[] = "101~MS-DRG Coding Validation Sepsis~Srinivas~7/5/2021";
    $recs[] = "102~MS-DRG Coding Validation MRSA~Srinivas~7/5/2021";
    $recs[] = "103~MS-DRG Coding Validation Coronavirus~Srinivas~7/5/2021";
    $recs[] = "104~MS-DRG Coding Validation Pneumonia~Srinivas~7/5/2021";
    $recs[] = "105~MS-DRG Coding Validation Acute Bronchitis~Srinivas~7/5/2021";
    $recs[] = "106~MS-DRG Coding Validation Lower Respiratory Infection~Srinivas~7/5/2021";
    $recs[] = "107~MS-DRG Coding Validation Acute Respiratory Distress Syn~Srinivas~7/5/2021";
    $recs[] = "108~MS-DRG Coding Validation Acute Respiratory Failure~Srinivas~7/5/2021";
    $recs[] = "109~MS-DRG Coding Validation HIV~Srinivas~7/5/2021";
    $recs[] = "100~MS-DRG Coding Validation HIV~Srinivas~7/5/2021";
    $recs[] = "101~MS-DRG Coding Validation Sepsis~Srinivas~7/5/2021";
    $recs[] = "102~MS-DRG Coding Validation MRSA~Srinivas~7/5/2021";
    $recs[] = "103~MS-DRG Coding Validation Coronavirus~Srinivas~7/5/2021";
    $recs[] = "104~MS-DRG Coding Validation Pneumonia~Srinivas~7/5/2021";
    $recs[] = "105~MS-DRG Coding Validation Acute Bronchitis~Srinivas~7/5/2021";
    $recs[] = "106~MS-DRG Coding Validation Lower Respiratory Infection~Srinivas~7/5/2021";
    $recs[] = "107~MS-DRG Coding Validation Acute Respiratory Distress Syn~Srinivas~7/5/2021";
    $recs[] = "108~MS-DRG Coding Validation Acute Respiratory Failure~Srinivas~7/5/2021";
    $recs[] = "109~MS-DRG Coding Validation HIV~Srinivas~7/5/2021";
    $recs[] = "100~MS-DRG Coding Validation HIV~Srinivas~7/5/2021";
    $recs[] = "101~MS-DRG Coding Validation Sepsis~Srinivas~7/5/2021";
    $recs[] = "102~MS-DRG Coding Validation MRSA~Srinivas~7/5/2021";
    $recs[] = "103~MS-DRG Coding Validation Coronavirus~Srinivas~7/5/2021";
    $recs[] = "104~MS-DRG Coding Validation Pneumonia~Srinivas~7/5/2021";
    $recs[] = "105~MS-DRG Coding Validation Acute Bronchitis~Srinivas~7/5/2021";
    $recs[] = "106~MS-DRG Coding Validation Lower Respiratory Infection~Srinivas~7/5/2021";
    $recs[] = "107~MS-DRG Coding Validation Acute Respiratory Distress Syn~Srinivas~7/5/2021";
    $recs[] = "108~MS-DRG Coding Validation Acute Respiratory Failure~Srinivas~7/5/2021";
    $recs[] = "109~MS-DRG Coding Validation HIV~Srinivas~7/5/2021";


    return $recs;
}

?>

