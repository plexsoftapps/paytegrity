<?php

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     testing
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 *
 */
	define('ENVIRONMENT', 'development');

?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Programs</title>
<link href="layout.css" rel="stylesheet" type="text/css">
</head>

<body style="background-color:aliceblue">

    <?php 
        // was an arg sent?
        $q = $_GET['q'];
        $title=$q;  //"Report not yet created";
        $reportcaption = "No Report Yet for the Query " . $q;
        require_once('header.php');
    ?>

    <?php
        
        // get the queries
        require_once('programsclass.php');
        $programsobject = new Programs();
        $programs = $programsobject->GetPrograms();  //array("Test1", "Test2", "Test3");  //
        $numcols = 4;
        $numrows = count($programs) / $numcols;
    ?>

    <br/>
    <div style="width:100%; height:80%;">
        <table border="1" style="width:95%; height:20px;" cellpadding="10,10,10,10">
            <tr style="height: 100%; color:blue; background-color:lightblue;">
                <td style="width:30%; font-size:26px; text-align:left;">
                Folders
                </td>
                <td style="width:70%; font-size:26px; text-align:left;">
                Report Headers Here
                </td>
            </tr>

        </table>

		<table border="1" style="width:95%; height:80%;" cellpadding="10,10,10,10">
			<tr>
            <td style="width:30%">
                    <div style="overflow-y:scroll; overflow-x: hidden; height:100%">
                        <?php
                            // show all the folder items
                            $num_programs = count($programs);
                            for ($i = 0; $i < $num_programs; $i++) 
                            {
                                $program = $programs[$i];
                                $parts = explode("~", $program);
                                if (count($parts) > 1)
                                {
                                    $link = $parts[1];
                                }
                                else 
                                {
                                    $link = "queriesnotyet.php?q=" . $parts[0];
                                }

                                echo('<a href="' . $baseurl . '/' . $link . '">' .
                                            $parts[0] . ' Queries <br/><br/>
                                        </a>');
                                
                            }
                        ?>
                    </div>
				</td>
				<td style="width:70%">
                    <div style="overflow-y:scroll; overflow-x: hidden; height:100%">
                        <?php echo($reportcaption); ?>
                    </div>
				</td>
			</tr>
		</table>
	</div>
	
    <?php 
        require_once('footer.php');
    ?>
</body>
</html>

<php?
?>

