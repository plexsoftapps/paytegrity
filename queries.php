<?php

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     testing
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 *
 */
	define('ENVIRONMENT', 'development');

?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Programs</title>
<link href="layout.css" rel="stylesheet" type="text/css">
</head>

<body class="body">

    <?php 
        $title="Programs";
        require_once('header.php');
    ?>

    <?php
        // get the programs
        require_once('programsclass.php');
        $programsobject = new Programs();
        $programs = $programsobject->GetPrograms();  //array("Test1", "Test2", "Test3");  //
        $numcols = 4;
        $numrows = count($programs) / $numcols;
    ?>

    <div style="width:100%; height:60%;">
		<table align="center">
			<tr>
				<td>
					<table align="center" cellspacing="10" cellpadding="10" style="table-layout:fixed; width: 100%;">
						<tr style="height: 10px;"></tr>
                        <?php 
                        //create the columns
                        $row = 0;
                        $col = 0;
                        // foreach ($programs as $program)
                        $num_programs = count($programs);
                        for ($i = 0; $i < $num_programs; $i++) 
                        {
                            echo('<tr style="height:10px;">');
                                // do 4 programs
                                echo("<td></td>");

                                for ($j = 0; $j<4; $j++)
                                {
                                    echo('<td style="font-size:26px; background-color:lightgrey; text-align:center;">');
                                    // program
                                    $program = $programs[$i + $j];
                                    $parts = explode("~", $program);
                                    if (count($parts) > 1)
                                    {
                                        echo('<a href="' . $baseurl . '/' . $parts[2] . '">
                                                <strong>' . $parts[1] . '<strong>
                                            </a>');
                                    }
                                    else 
                                    {
                                        echo('<strong>' . $parts[0] . '<strong>');
                                    }
                                    echo('</td>');
                                }
                                echo("<td></td>");
                                $i=$i+3;
                            echo('</tr>');
                        }
                        ?>
					</table>
				</td>
			</tr>
		</table>
	</div>
	
    <?php 
        require_once('footer.php');
    ?>
</body>
</html>

<php?
?>

