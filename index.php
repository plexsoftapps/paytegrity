<?php

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     testing
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 *
 */
	define('ENVIRONMENT', 'development');

?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Main Page</title>
<link href="layout.css" rel="stylesheet" type="text/css">
</head>

<body class="body">

    <?php 
        $title="Home";
        require_once('header.php');
    ?>

    <div style="width:100%; height:80%;">
		<table align="center">
			<tr>
				<td>
					<table align="center" cellspacing="3" cellpadding="3" style="table-layout:fixed; width: 100%;">
						<tr style="height: 100px;"></tr>
						<tr>
							<td align="center">
								<a href="<?php echo($baseurl);?>/queries.php">
									<img src="queries.png" alt="" id="mainbutton"/>
								</a>
							</td>
							<td align="center">
								<a href="<?php echo($baseurl);?>/forms.php">
									<img src="forms.png" alt="" id="mainbutton"/>
								</a>
							</td>
							<td align="center">
								<a href="<?php echo($baseurl);?>/reports.php">
									<img src="reports.png" alt="" id="mainbutton"/>
								</a>
							</td>
							<td align="center">
								<a href="<?php echo($baseurl);?>/programforms.php">
									<img src="programforms.png" alt="" id="mainbutton"/>
								</a>
							</td>
							<td align="center">
								<a href="<?php echo($baseurl);?>/conference.php">
									<img src="conference.png" alt="" id="mainbutton"/>
								</a>
							</td>
							<td align="center">
								<a href="<?php echo($baseurl);?>/training.php">
									<img src="training.png" alt="" id="mainbutton"/>
								</a>
							</td>
						</tr>
						<tr>
							<td align="center" style="margin: 10px;"><strong>Queries</strong><br/>Select Programs</td>
							<td align="center" style="margin: 10px;"><strong>Forms</strong><br/>Create, Edit, Manage</td>
							<td align="center" style="margin: 10px;"><strong>Reports</strong><br/>Performance, Management</td>
							<td align="center" style="margin: 10px;"><strong>Program Forms</strong><br/>Program Forms</td>
							<td align="center" style="margin: 10px;"><strong>Conference</strong><br/>Schedule Meeting</td>
							<td align="center" style="margin: 10px;"><strong>Training</strong><br/>Training & Development</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	
    <?php 
        require_once('footer.php');
    ?>
</body>
</html>

<php?
?>

